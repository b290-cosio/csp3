// import { Fragment } from 'react';
import { useState, useEffect } from 'react';
import { Container } from 'react-bootstrap';
import { BrowserRouter as Router } from 'react-router-dom';
import { BrowserRouter, Routes, Route } from 'react-router-dom';

import AppNavbar from './components/AppNavbar';
  //already imported to Home.js
// import Banner from './components/Banner';
// import Highlights from './components/Highlights';
import Products from './pages/Products';
import ProductView from './pages/ProductView'
import Home from './pages/Home';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout'
import ErrorPage from './pages/ErrorPage';
import Dashboard from './pages/Admin/Dashboard';
import CreateProduct from './pages/Admin/Create-Product';
import UpdateProduct from './pages/Admin/Update-Product';
import OrderHistory from './pages/OrderHistory'





import './App.css';
import { UserProvider } from './UserContext'



function App() {
  
  const [user, setUser] = useState({ id : null, isAdmin : null });

  const unsetUser = () => {
    localStorage.clear();
  }

  useEffect(() => {
    if (localStorage.getItem('token')) {
      fetch('http://localhost:4000/users/details', {
        headers: {
          Authorization: `Bearer ${localStorage.getItem('token')}`
        }
      })
        .then(res => res.json())
        .then(data => {
          console.log(data);

            setUser({
              id: data._id,
              isAdmin: data.isAdmin
            });
        
        });
    }

    console.log(user);
    console.log(localStorage);
  }, []);

  return (
    <UserProvider value={{ user, setUser, unsetUser }}>
      <Router>
        <Container fluid>
          <AppNavbar />
          <Container>
            <Routes>
              <Route path="/" element={<Home />} />
              <Route path="/Products/all" element={<Products />} />
              <Route path="/products/:productId" element={<ProductView />} />
              <Route path="/register" element={<Register />} />
              <Route path="/login" element={<Login />} />
              <Route path="/logout" element={<Logout />} />
              <Route path="/order-history" element={<OrderHistory />} />
              <Route path="/*" element={<ErrorPage />} />
              <Route path="/admin/dashboard" element={<Dashboard />} />
              <Route path="/admin/product/add" element={<CreateProduct />} />
              <Route path="/admin/product/update" element={<UpdateProduct />} />
            </Routes>
          </Container>
        </Container>
      </Router>
    </UserProvider>
    // <Fragment>      
    //   <AppNavbar />
    //   <Banner />
    // </Fragment>

    // <>      
    //   <AppNavbar />
    //   <Container>
    //     {/*  <Home />
    //       <Courses />*/}
    //   {/*<Register />*/}
    //   <Login />
    //   </Container>      
    // </>
  );
}

export default App;


