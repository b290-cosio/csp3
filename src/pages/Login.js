import { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import { Navigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function Login() {


    const { user, setUser } = useContext(UserContext)

  const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');

    const [isActive, setIsActive] = useState(true);

 function authenticate(e) {
  e.preventDefault();

  fetch("https://csp-2.onrender.com/users/login", {
    method: "POST",
    headers: {
      "Content-Type": "application/json"
    },
    body: JSON.stringify({
      email: email,
      password: password
    })
  })
  .then(res => res.json())
  .then(data => {
    console.log("Login response:", data);

    if (typeof data.access !== "undefined") {
      localStorage.setItem('token', data.access);
      console.log("Access token:", data.access);
      retrieveUserDetails(data.access);
      Swal.fire({
        icon: 'success',
        title: 'Login successful!',
        text: 'Welcome to my shop!!',
      });
    } else {
      Swal.fire({
        icon: 'error',
        title: 'Authentication failed',
        text: 'Please check your login credentials and try again.'
      });
    }
  })
  .catch(error => {
    console.log("Login error:", error);
  });

  setEmail('');
  setPassword('');
}

const retrieveUserDetails = (token) => {
  fetch("https://csp-2.onrender.com/users/details", {
    headers: {
      Authorization: `Bearer ${token}`
    }
  })
  .then(res => {
    console.log("User details response:", res);
    return res.json();
  })
  .then(data => {
    console.log("User details data:", data);
    setUser({
      id: data._id,
      isAdmin: data.isAdmin
    });
  })
  .catch(error => {
    console.log("User details error:", error);
  });
};

useEffect(() => {
  // Validation to enable submit button when all fields are populated and both passwords match
  if (email !== '' && password !== '') {
    setIsActive(true);
  } else {
    setIsActive(false);
  }
}, [email, password]);


   return (
     (user.id) ?
       <Navigate to="/" />
     :        
       <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', height: '100vh' }}>
      <Form style={{ width: '400px' }} onSubmit={(e) => authenticate(e)}>
        <Form.Group controlId="userEmail">
          <Form.Label className="text-light">Email address</Form.Label>
          <Form.Control
            type="email"
            placeholder="Enter email"
            value={email}
            onChange={(e) => setEmail(e.target.value)}
            required
          />
        </Form.Group>

        <Form.Group controlId="password">
          <Form.Label className="text-light">Password</Form.Label>
          <Form.Control
            type="password"
            placeholder="Password"
            value={password}
            onChange={(e) => setPassword(e.target.value)}
            required
          />
        </Form.Group>

        {isActive ? (
          <Button variant="primary" type="submit" id="submitBtn" className="mt-2">
            Login
          </Button>
        ) : (
          <Button variant="danger" type="submit" id="submitBtn" className="mt-2" disabled>
            Login
          </Button>
        )}
      </Form>
    </div>
  );
};

