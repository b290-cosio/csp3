import React, { useEffect, useState, useContext } from 'react';
import UserContext from '../UserContext';
import OrderCard from '../components/OrderCard';

export default function OrderHistory() {
  const { user } = useContext(UserContext);
  const [orders, setOrders] = useState([]);

  useEffect(() => {
    const fetchOrders = async () => {
      try {
        const response = await fetch('https://csp-2.onrender.com/users/myorders', {
          headers: {
            Authorization: `Bearer ${localStorage.getItem('token')}`
          }
        });

        if (response.ok) {
          const data = await response.json();
          setOrders(data);
          fetchProductDetails(data); // Call the defined function here
        } else {
          console.error('Failed to fetch orders');
        }
      } catch (error) {
        console.error('Error fetching orders:', error);
      }
    };

    fetchOrders();
  }, []);

 const fetchProductDetails = async (orders) => {
  try {
    const updatedOrders = await Promise.all(
      orders.map(async (order) => {
        const { productId, quantity, totalPrice } = order;

   
        if (productId) {
          const response = await fetch(`https://csp-2.onrender.com/products/${productId}`);
          const productData = await response.json();

          return {
            ...order,
            productDetails: productData,
            quantity: quantity,
            totalPrice: totalPrice
          };
        } else {
          
          return order; 
        }
      })
    );

    setOrders(updatedOrders);
  } catch (error) {
    console.error('Error fetching product details:', error);
  }
};


  return (
    <div>
      <h2 className="text-light text-center">Order History</h2>
      {orders.length > 0 ? (
        <div>
          {orders.map((order, index) => (
            <OrderCard key={index} order={order} />
          ))}
        </div>
      ) : (
        <p>No orders found.</p>
      )}
    </div>
  );
}
