import React from 'react';
import { ErrorBanner } from '../components/Banner';
import { Link } from 'react-router-dom';

function ErrorPage() {
  return (
    <div>
      <h1 className="text-light">Oops! Page Not Found</h1>
      <p>Go back to the <a href="/">Homepage</a>.</p>
    </div>
  );
}

export default ErrorPage;