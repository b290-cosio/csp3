import { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import { Navigate } from 'react-router-dom';
import { useParams, useNavigate, Link } from 'react-router-dom';
import UserContext from '../../UserContext';
import Swal from 'sweetalert2';
import Table from 'react-bootstrap/Table'

export default function UpdateProduct() {
	const { user, setUser } = useContext(UserContext)
	const [ products, setProducts ] = useState([])
	const productId = localStorage.getItem("productId");


	const [productName, setProductName] = useState('');
	const [description, setDescription] = useState('');
	const [price, setPrice] = useState('');

	const navigate = useNavigate();

	useEffect(() => {
		fetch('https://csp-2.onrender.com/products/', {
			method: "POST",
    		headers: {
	        		"Content-Type": "application/json"
	        },
    		body: JSON.stringify({
    			id: productId
    		})
		}).then(res => res.json()).then(data => setProducts(data));
	}, [])

	// Function Start
	function createProduct (e) {
		// Prevents page redirection via form submission
    	e.preventDefault();

    	fetch('https://csp-2.onrender.com/products/add', {
    		method: "POST",
    		headers: {
    			Authorization: `Bearer ${ localStorage.getItem('token') }`,
    			"Content-Type": "application/json"
    		},
    		body: JSON.stringify({
    			name: productName,
    			description: description,
    			price: price
    		})
    	}).then(res => res.json()).then(data => { 
    		Swal.fire({
                icon: 'success',
                title: 'Product created!',
                timer: 2000
            }).then((result) => {
		  		if (result.isConfirmed) {
		  			location.reload(true);
		  		}
		  	})
    	})
	}

	return (
  (user.isAdmin) ?
  <>
    <h3 className="text-light">Create new product</h3>
    <hr />
    <Form onSubmit={(e) => createProduct(e)}>
      {/* Product Name */}
      <Form.Group controlId="productName">
        <Form.Label className="text-light">Product Name</Form.Label>
        <Form.Control
          type="text"
          placeholder="Enter product name"
          value={productName}
          onChange={e => setProductName(e.target.value)}
          required
        />
      </Form.Group>

      <Form.Group controlId="productDescription">
        <Form.Label className="text-light">Product Description</Form.Label>
        <Form.Control
          type="text"
          placeholder="Enter product description"
          value={description}
          onChange={e => setDescription(e.target.value)}
          required
        />
      </Form.Group>

      <Form.Group controlId="productPrice">
        <Form.Label className="text-light">Product Price</Form.Label>
        <Form.Control
          type="number"
          placeholder="Enter product price"
          value={price}
          onChange={e => setPrice(e.target.value)}
          required
        />
      </Form.Group>

      <Button variant="success" type="submit" className="mt-2" to="/admin/dashboard">Register</Button>
    </Form>
  </>
  :
  <Navigate to="/admin/dashboard" />
)
}
