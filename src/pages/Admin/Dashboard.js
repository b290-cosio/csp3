import ProductTable from '../../components/ProductList';
import { Link } from 'react-router-dom';
import { Button, Container } from 'react-bootstrap';
import Swal from 'sweetalert2';
import { useState, useEffect } from 'react';

export default function AdminDashboard() {
  const [products, setProducts] = useState([]);

  useEffect(() => {
    fetch(`https://csp-2.onrender.com/products/all`, {
      headers: {
        authorization: `Bearer ${localStorage.getItem('token')}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        setProducts(
          data.map((product) => (
            <ProductTable key={product._id} product={product} />
          ))
        );
      })
      .catch((err) => console.log(err));

    return () => {
     
    };
  }, []);

  function deleteAll(e) {
    e.preventDefault();

    Swal.fire({
      title: 'Are you sure?',
      text: "Delete All Products?'",
      icon: 'warning',
      showCancelButton: true,
    }).then((result) => {
      if (result.isConfirmed) {
        fetch(`https://csp-2.onrender.com/products/delete`, {
          method: 'DELETE',
          headers: {
            Authorization: `Bearer ${localStorage.getItem('token')}`,
            'Content-Type': 'application/json',
          },
        })
          .then((res) => res.json())
          .then((data) => data)
          .then(() => {
            setProducts([]);
            Swal.fire({
              title: 'Deleted!',
              text: 'All products are now deleted!',
              icon: 'success',
            });
          });
      }
    });
  }

  return (
    <div className="bg-dark min-vh-100">
      <Container fluid className="py-4">
        <h2 className="text-center text-white">Admin Dashboard</h2>
        <Link to="/admin/product/add">
          <Button variant="secondary" className="me-2">
            Create New Product
          </Button>
        </Link>
        <hr />
        <h3 className="text-center text-white">Products Registered</h3>

        <div className="table-responsive">
          <table className="table table-striped table-hover bg-white">
            <thead>
              <tr>
                <th width="200">Id</th>
                <th width="200">Name</th>
                <th width="200">Description</th>
                <th width="100">Price</th>
                <th width="100">isActive</th>
                <th width="200">createdOn</th>
                <th width="290">Actions</th>
              </tr>
            </thead>
            <tbody>{products}</tbody>
          </table>
        </div>
      </Container>
    </div>
  );
}