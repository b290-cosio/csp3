import { useState, useEffect, useContext } from 'react';
import Swal from 'sweetalert2';
import { Form, Button } from 'react-bootstrap';
import { Navigate } from 'react-router-dom';
import { useParams, useNavigate, Link } from 'react-router-dom';
import Table from 'react-bootstrap/Table';
import UserContext from '../../UserContext';


export default function UpdateProduct() {
  const navigate = useNavigate();
  const [isProductUpdated, setProductUpdated] = useState(false);
  const { user, setUser } = useContext(UserContext);
    const [product, setProduct] = useState(null);
  const productId = localStorage.getItem("productId");


  const [productName, setProductName] = useState('');
  const [description, setDescription] = useState('');
  const [price, setPrice] = useState('');


  useEffect(() => {
      fetch(`https://csp-2.onrender.com/products/${productId}`)
        .then(res => res.json())
        .then(data => {
          setProduct(data);
          setProductName(data.name);
          setDescription(data.description);
          setPrice(data.price);
        })
        .catch(error => {
          console.log('Error fetching product:', error);
        });
  }, [productId]);

  function updateProduct(e) {
    e.preventDefault();

    Swal.fire({
      title: 'Are you sure?',
      text: "Update Product?",
      icon: 'warning',
      showCancelButton: true,
    }).then((result) => {
      if (result.isConfirmed) {
        fetch(`https://csp-2.onrender.com/products/${productId}`, {
          method: "PUT",
          headers: {
            Authorization: `Bearer ${localStorage.getItem('token')}`,
            "Content-Type": "application/json"
          },
          body: JSON.stringify({
            name: productName,
            description: description,
            price: price
          })
        })
          .then(res => res.json())
          .then(data => {
            Swal.fire({
              title: 'Updated!',
              text: 'Product updated!',
              icon: 'success',
            }).then(() => {
              setProductUpdated(true);
              window.location.reload();
            });
          })
          .catch(error => {
            console.log('Error updating product:', error);
          });
      }
    });
  }

  return (
    <>
    
      <hr />

        <Form onSubmit={updateProduct}>
        <Form.Group controlId="productName">
          <Form.Label className="text-light">Product Name</Form.Label>
          <Form.Control
            type="text"
            placeholder="Enter product name"
            value={productName}
            onChange={e => setProductName(e.target.value)}
            required
          />
        </Form.Group>

        <Form.Group controlId="productDescription">
          <Form.Label className="text-light">Product Description</Form.Label>
          <Form.Control
            type="text"
            placeholder="Enter product description"
            value={description}
            onChange={e => setDescription(e.target.value)}
            required
          />
        </Form.Group>

        <Form.Group controlId="productPrice">
          <Form.Label className="text-light">Product Price</Form.Label>
          <Form.Control
            type="number"
            placeholder="Enter product price"
            value={price}
            onChange={e => setPrice(e.target.value)}
            required
          />
        </Form.Group>

        <Button variant="success" type="submit" className="mt-2">
          Update
        </Button>
        <Button variant="danger" type="button" className="mt-2" onClick={() => navigate('/admin/dashboard')}>
          Cancel
        </Button>
      </Form>
    </>
  );
}
