import { useState, useEffect, useContext } from 'react';
import { Container, Card, Button, Col, Form } from 'react-bootstrap';
import { useParams, Link, useNavigate } from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';

export default function ProductView() {
  const { productId } = useParams();
  const { user } = useContext(UserContext);
  const navigate = useNavigate();

  const [name, setName] = useState('');
  const [description, setDescription] = useState('');
  const [price, setPrice] = useState(0);
  const [quantity, setQuantity] = useState(1);
  const [totalPrice, setTotalPrice] = useState(price);

  const handleQuantityChange = (event) => {
    const quantityValue = parseInt(event.target.value);

    if (!isNaN(quantityValue) && quantityValue >= 1) {
      setQuantity(quantityValue);
      setTotalPrice(price * quantityValue);
    }
  };

  const handleCheckout = () => {
    if (user.isAdmin) {
      Swal.fire({
        icon: 'warning',
        title: 'Admins cannot checkout',
        text: 'Only regular users can checkout.',
      });
    } else {
      const order = {
        productId: productId,
        quantity: quantity,
        totalPrice: totalPrice
      };

      fetch('https://csp-2.onrender.com/orders/checkout', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${localStorage.getItem('token')}`
        },
        body: JSON.stringify(order)
      })
        .then((res) => res.json())
        .then((data) => {
          console.log(data);

          if (data === true) {
            Swal.fire({
              icon: 'success',
              title: 'Successfully added',
              text: 'You have successfully added the product to your cart.'
            });

          } else {
            Swal.fire({
              icon: 'error',
              title: 'Something went wrong',
              text: 'Please try again.'
            });
          }
        });
    }
  };

  useEffect(() => {
    fetch(`https://csp-2.onrender.com/products/${productId}`)
      .then((res) => res.json())
      .then((data) => {
        setName(data.name);
        setDescription(data.description);
        setPrice(data.price);
        setTotalPrice(data.price);
      });
  }, [productId]);

 const handleIncrement = () => {
  setQuantity(quantity + 1);
};

const handleDecrement = () => {
  if (quantity > 1) {
    setQuantity(quantity - 1);
  }
};

useEffect(() => {
  const calculateTotalPrice = () => {
    const updatedTotalPrice = price * quantity;
    setTotalPrice(updatedTotalPrice);
  };

  calculateTotalPrice();
}, [price, quantity]);

return (
  <Container className="mt-5 justify-content-center text-center">
    <Col lg={{ span: 6, offset: 3 }}>
      <Card>
        <Card.Body className="text-center">
          <Card.Title>{name}</Card.Title>

          <Card.Subtitle>Description:</Card.Subtitle>
          <Card.Text>{description}</Card.Text>

          <Card.Subtitle>Price:</Card.Subtitle>
          <Card.Text>PhP {price}</Card.Text>

          <Form.Group controlId="quantity">
            <Form.Label>Quantity:</Form.Label>
            <div className="input-group">
              <button className="btn btn-outline-primary" onClick={handleDecrement}>
                -
              </button>
              <Form.Control
                type="number"
                value={quantity}
                onChange={handleQuantityChange}
                min={1}
              />
              <button className="btn btn-outline-primary" onClick={handleIncrement}>
                +
              </button>
            </div>
          </Form.Group>

          <Card.Subtitle>Total Price:</Card.Subtitle>
          <Card.Text>PhP {totalPrice}</Card.Text>

          {user.id ? (
           <div>
               <Button variant="primary" block onClick={handleCheckout}>
                 Checkout
               </Button>
               <Link to="/products/all" className="btn btn-secondary">
                 Go Back
               </Link>
             </div>
          ) : (
            <Link className="btn btn-danger btn-block" to="/login">
              Log in to add
            </Link>
          )}
        </Card.Body>
      </Card>
    </Col>
  </Container>
);
}
