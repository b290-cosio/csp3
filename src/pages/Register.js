import { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import { Navigate, useNavigate, useHistory  } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';



export default function Register() {

  const { user } = useContext(UserContext);

  const navigate = useNavigate();

  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [email, setEmail] = useState('');
  const [mobileNo, setMobileNo] = useState('');
  const [password1, setPassword1] = useState('');
    const [password2, setPassword2] = useState('');

    const [isActive, setIsActive] = useState(false);
    
  
    function registerUser(e) {

      e.preventDefault();

      fetch('https://csp-2.onrender.com/users/checkEmail', {
          method: "POST",
          headers: {
              'Content-Type': 'application/json'
          },
          body: JSON.stringify({
              email: email
          })
      })
      .then(res => res.json())
      .then(data => {

          console.log(data);

          if(data == true){

            Swal.fire({
              title: 'Duplicate email found',
              icon: 'error',
              text: 'Kindly provide another email to complete the registration.'  
            });

          } else {

                fetch("https://csp-2.onrender.com/users/register", {
                    method: "POST",
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({
                        firstName: firstName,
                        lastName: lastName,
                        email: email,
                        mobileNo: mobileNo,
                        password: password1
                    })
                })
                .then(res => res.json())
                .then(data => {

                    console.log(data);

                    if (data) {

                        setFirstName('');
                        setLastName('');
                        setEmail('');
                        setMobileNo('');
                        setPassword1('');
                        setPassword2('');

                        Swal.fire({
                            title: 'Registration successful',
                            icon: 'success',
                            text: 'Welcome to Cosh Tech Shop!'
                        });

                        navigate("/login");

                    } else {

                        Swal.fire({
                            title: 'Something wrong',
                            icon: 'error',
                            text: 'Please try again.'   
                        });

                    }
                })
            };

      });

      setFirstName('');
      setLastName('');
      setEmail('');
      setMobileNo('');
      setPassword1('');
      setPassword2('');

    }

    useEffect(() => {
      if((firstName !== '' && lastName !== '' && email !== '' && mobileNo.length === 11 && password1 !== '' && password2 !== '') && (password1 === password2)){
        setIsActive(true);
      } else {
        setIsActive(false);
      }
    }, [firstName, lastName, email, mobileNo, password1, password2]);

  return (
    <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', height: '100vh' }}>
      <Form style={{ width: '400px' }} onSubmit={(e) => registerUser(e)}>
        <Form.Group controlId="firstName">
          <Form.Label className="text-light">First Name</Form.Label>
          <Form.Control
            type="text"
            placeholder="Enter first name"
            value={firstName}
            onChange={e => setFirstName(e.target.value)}
            required
          />
        </Form.Group>

        <Form.Group controlId="lastName">
          <Form.Label className="text-light">Last Name</Form.Label>
          <Form.Control
            type="text"
            placeholder="Enter last name"
            value={lastName}
            onChange={e => setLastName(e.target.value)}
            required
          />
        </Form.Group>

        <Form.Group controlId="userEmail">
          <Form.Label className="text-light">Email Address</Form.Label>
          <Form.Control
            type="email"
            placeholder="Enter an email"
            value={email}
            onChange={e => setEmail(e.target.value)}
            required
          />
          <Form.Text className="text-muted text-light">We'll never share your email with anyone.</Form.Text>
        </Form.Group>

        <Form.Group controlId="mobileNo">
          <Form.Label className="text-light">Mobile Number</Form.Label>
          <Form.Control
            type="text"
            placeholder="Enter Mobile Number"
            value={mobileNo}
            onChange={(e) => {
              const input = e.target.value.replace(/[^0-9]/g, '').slice(0, 11);
              setMobileNo(input);
            }}
            required
          />
        </Form.Group>

        <Form.Group controlId="password1">
          <Form.Label className="text-light">Password</Form.Label>
          <Form.Control
            type="password"
            placeholder="Enter a password"
            value={password1}
            onChange={e => setPassword1(e.target.value)}
            required
          />
        </Form.Group>

        <Form.Group controlId="password2">
          <Form.Label className="text-light">Confirm Password</Form.Label>
          <Form.Control
            type="password"
            placeholder="Confirm your password"
            value={password2}
            onChange={e => setPassword2(e.target.value)}
            required
          />
        </Form.Group>

        {isActive ? (
          <Button variant="primary" type="submit" id="submitBtn" className="mt-2 text-light">
            Register
          </Button>
        ) : (
          <Button variant="primary" type="submit" id="submitBtn" className="mt-2 text-light" disabled>
            Register
          </Button>
        )}
      </Form>
    </div>
  );
};