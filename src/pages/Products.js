import React, { useEffect, useState } from 'react';
import ProductCard from '../components/ProductCard';

export default function Products() {
  const [products, setProducts] = useState([]);

  useEffect(() => {
    fetch('https://csp-2.onrender.com/products/')
      .then(res => res.json())
      .then(data => {
        if (Array.isArray(data)) {
          setProducts(data);
        } else {
          console.error('Invalid data format:', data);
        }
      })
      .catch(error => {
        console.error('Error fetching products:', error);
      });
  }, []);

  return (
    <>
      <h2 className="text-light text-center">Available Products on Sale</h2>
       {products.map(product => (
        <ProductCard
          key={product.id}
          product={product}
          handleCheckout={() => handleCheckout(product.id)}
        />
      ))}
    </>
  );
}
