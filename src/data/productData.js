const productsData = [
	{
		id : "647eefaecab42bcd90c91109",
		name : "Glorious Model O wireless",
		description : "The Glorious Model O Wireless Mouse is a high-performance gaming mouse that offers an exceptional wireless gaming experience. It features a sleek and lightweight design, weighing only 69 grams, making it incredibly agile and comfortable to use during intense gaming sessions.",
		price : 5500
		imageUrl : 'https://cdn.shopify.com/s/files/1/2227/7667/products/ModelOWirelessw_1024x1024.png?v=1608613047'
	}


	]

export default productsData;