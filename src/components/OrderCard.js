import React from 'react';

export default function OrderCard  ({ order })  {
  const { _id, productId, quantity, totalPrice } = order;

  return (
    <div className="order-card">
      <table className="table">
        <tbody>
          <tr>
            <td>Order ID:</td>
            <td>{_id}</td>
          </tr>
          <tr>
            <td>Product ID:</td>
            <td>{productId}</td>
          </tr>
          <tr>
            <td>Quantity:</td>
            <td>{quantity}</td>
          </tr>
          <tr>
            <td>Total Price:</td>
            <td>{order.totalPrice}</td>
          </tr>
        </tbody>
      </table>
    </div>
  );
};

