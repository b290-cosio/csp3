import React from 'react';
import Card from 'react-bootstrap/Card';
import { Link } from 'react-router-dom';

export default function ProductCard({ product }) {
  const { _id, name, description, price, imageURL, } = product;

  return (
  <div className="mt-4">
    <div className="row justify-content-center">
      <div>
        <Card>
          <Card.Body>
            <Card.Title>{name}</Card.Title>
            <Card.Subtitle>Description:</Card.Subtitle>
            <Card.Text>{description}</Card.Text>
            <Card.Subtitle>Price:</Card.Subtitle>
            <Card.Text>PhP {price}</Card.Text>
            <Link className="btn btn-primary" to={`/products/${_id}`}>
              View
            </Link>
            <Link className="btn btn-dark" to={`/`}>
              Go Back
            </Link>
          </Card.Body>
        </Card>
      </div>
    </div>
  </div>
);
}

export function OrderHistory({ orders }) {
  return (
    <div>
      {orders.map((order) => (
        <ProductCard key={order._id} product={order} />
      ))}
    </div>
  );
}