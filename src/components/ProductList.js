import { useState, useEffect, useContext } from 'react';
import { Form, Button, Row, Col, Card } from 'react-bootstrap';
import { Navigate } from 'react-router-dom';
import { useParams, useNavigate, Link } from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';
import Table from 'react-bootstrap/Table';

export default function AdminProduct({ product }) {
  const { user, setUser } = useContext(UserContext);
  const [productId, setProductId] = useState('');
  const { _id, name, description, price, isActive, createdOn } = product;

  function updateProduct() {
    localStorage.setItem('productId', product._id);
  }

 function activateProduct() {
  Swal.fire({
    title: 'Are you sure?',
    text: 'Activate product?',
    icon: 'warning',
    showCancelButton: true
  }).then((result) => {
    if (result.isConfirmed) {
      fetch(`https://csp-2.onrender.com/products/${product._id}/activate`, {
        method: 'PATCH',
        headers: {
          Authorization: `Bearer ${localStorage.getItem('token')}`
        }
      })
        .then(res => res.json())
        .then(data => {
          Swal.fire({
            title: 'Activated!',
            text: 'Product set to active!',
            icon: 'success'
          }).then(() => {
            window.location.reload(); // Refresh the page
          });
        });
    }
  });
}

  function disableProduct() {
    Swal.fire({
      title: 'Are you sure?',
      text: 'Disable product?',
      icon: 'warning',
      showCancelButton: true
    }).then((result) => {
      if (result.isConfirmed) {
        fetch(`https://csp-2.onrender.com/products/${product._id}/archive`, {
          method: 'PATCH',
          headers: {
            Authorization: `Bearer ${localStorage.getItem('token')}`
          }
        })
          .then(res => res.json())
          .then(data => {
            Swal.fire({
              title: 'Deactivated!',
              text: 'Product is disabled!',
              icon: 'success'
            }).then(() => {
              window.location.reload(); // Refresh the page
            });
          });
      }
    });
  }

  return (
    <>
      <tr>
        <td>{product._id}</td>

        <td>{product.name}</td>
        <td>{product.description}</td>
        <td>{product.price}</td>
        {isActive ? <td>True</td> : <td>False</td>}
        <td>{product.createdOn}</td>
        <td>
          <Link to="/admin/product/update">
            <Button className="adminBtn" variant="warning" onClick={updateProduct}>
              Update
            </Button>
          </Link>
          {product.isActive ? (
            <Button className="adminBtn" variant="danger" onClick={disableProduct}>
              Disable
            </Button>
          ) : (
            <Button className="adminBtn" variant="success" onClick={activateProduct}>
              Enable
            </Button>
          )}
        </td>
      </tr>
    </>
  );
}
