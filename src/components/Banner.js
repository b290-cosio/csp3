import { Button, Col, Row } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function Banner() {
	return (
		
		<Row>
			<Col className="p-5">
				<h1 id="title" className=" text-center">COSH TECH SHOP</h1>
				<p className="text-light text-center">We Have All the Gears that you need!</p>
			</Col>
		</Row>
	)
}
